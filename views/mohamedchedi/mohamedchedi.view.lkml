include: "/views/vin.view.lkml"
view: +vin {
measure:  countbrand {
  type:  count_distinct
  sql:  ${brand} ;;
  label: " Unique count brand"
}
#Ex1
measure: countmodel {
  type:  count_distinct
  sql:  ${model} ;;
}
#Ex1
measure: countmodel {
  type:  count_distinct
  sql:  ${model} ;;
  label: " Unique count model"
  drill_fields: [model]
}
#Ex2
dimension: dealer_name_1 {
  type:  string
  sql:  REPLACE(${dealer_name}, " ", "_") ;;
}
#Ex3
dimension: fuel_type_new {
  type:  string
  sql:  CASE
      WHEN ${fuel_type} IN ('DIESEL') THEN 'Gasoil'
      WHEN ${TABLE}.fuel_type IN ('ELECTRIC') THEN 'Electrique'
      WHEN ${fuel_type} IN ('PETROL') THEN 'Essence'
      WHEN ${fuel_type} IN ('PETROL CNGGAZ') THEN 'GAZ'
      WHEN ${fuel_type} IN ('PETROL LPG') THEN 'GAZ'
  END;;
}
#Ex4
dimension: model_version_concat {
  type: string
  sql: CONCAT(${model}, ' ', ${version}) ;;
  description: "Concatenation de Model et Version"
  label: "Model and Version"
}
#Ex5
dimension: order_date_invoice_diff {
  type: number
  sql: DATEDIFF(${TABLE}.invoice_date, ${TABLE}.order_date) ;;
  description: "Difference in days between Order Date and Invoice Date"
}

#Ex6 (on le fait pas, le format est deja en date)
dimension_group:  orderDate{}

#Ex7
dimension: invoiceDATE {
  sql: FORMAT_DATETIME('%A %d %B %Y', ${invoice_date});;
  label: "invoiceDATE_EMA_exo7"
}

#Ex8
measure: min_catalogue_price {
  type: min
  sql: ${catalogue_price} ;;
  label: "Min Catalogue Price"
  value_format: "€#,##0.0"
}

measure: max_catalogue_price {
  type: max
  sql: ${catalogue_price} ;;
  label: "Max Catalogue Price"
  value_format: "€#,##0.0"
}

measure: avg_catalogue_price {
  type: average
  sql: ${catalogue_price} ;;
  label: "Avg Catalogue Price"
  value_format: "€#,##0.0"
}

#Ex9
dimension: order_invoice_days_diff {
  type: number
  sql: DATEDIFF(${invoice_date}, ${order_date}) ;;
  label: "Order Invoice Days Difference"
}

measure: min_order_invoice_days_diff {
  type: min
  sql: ${order_invoice_days_diff} ;;
  label: "Min Order Invoice Days Difference"
}

measure: max_order_invoice_days_diff {
  type: max
  sql: ${order_invoice_days_diff} ;;
  label: "Max Order Invoice Days Difference"
}

measure: avg_order_invoice_days_diff {
  type: average
  sql: ${order_invoice_days_diff} ;;
  label: "Avg Order Invoice Days Difference"
}

#Ex10
dimension: logo1 {
  type: string
  sql:  CASE
        WHEN ${brand} = 'ALPINE' THEN 'https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg'
        WHEN ${brand} = 'DACIA' THEN 'https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg'
        WHEN ${brand} = 'RENAULT' THEN 'https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg'
        END;;
        html: <img src="{{value}}" height='170' width='255'/>;;
}

dimension: logo2 {
  type:  string
  sql:  ${brand} ;;
  html:
  {% if brand._value == "ALPINE" %}
  <img src="https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg" height="170" width="255">
  {% elsif brand._value == "DACIA" %}
  <img src="https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg" height="170" width="255">
  {% else %}
  <img src="https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg" height="170" width="255">
  {% endif %};;
}

}
