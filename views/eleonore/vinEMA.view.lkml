include: "/views/vin.view.lkml"
view: +vin {
  #Exercice1
  measure: countModel_EMA {
    type: count_distinct
    sql: ${model};;
    label: "CountModel_EMA_exo1"
  }

  #Exercice2
  measure: countModel_EMA {
    type: count_distinct
    sql: ${model};;
    drill_fields: [model]
    label: "CountModel_drill_EMA_exo2"
  }

  #Exercice3
  dimension: dealer_name_hyphen_EMA {
    type: string
    sql: REPLACE(${dealer_name}," ", "-");;
    label: "dealer_name_hyphen_EMA_exo3"
    }

  #Exercice4
  dimension: fuel_type_replaced_EMA {
    type: string
    sql: CASE
      WHEN ${fuel_type} = 'DIESEL' THEN 'Gasoil'
      WHEN ${fuel_type} = 'ELECTRIC' THEN 'Electrique'
      WHEN ${fuel_type} = 'PETROL' THEN 'Essence'
      WHEN ${fuel_type} IN ('PETROL CNGGAZ', 'PETROL LPG') THEN 'GAZ'
      ELSE ${fuel_type} # Si aucune correspondance, conservez la valeur d'origine
    END ;;
    label: "fuel_type_replaced_EMA_exo4"
    }

  #Exercice5
  dimension: concat_model_version {
    type: string
    sql: CONCAT (${model},${version}) ;;
    drill_fields: [brand, model, version, catalogue_price]
    label: "concat_model_version_EMA_exo5"
    }

  #Exercice6
  dimension_group: orderDATE {
    type: time
    timeframes: [raw, date, week, month, quarter, year]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
    label: "orderDATE_EMA_exo6"
  }

  #Exercice7
  dimension: invoiceDATE {
    sql: FORMAT_DATETIME('%A %d %B %Y', ${invoice_date});;
    label: "invoiceDATE_EMA_exo7"
  }

  #Exercice8
  measure: catalogue_price_min {
    type: min
    sql: (${catalogue_price});;
    value_format: "#.0€;(e#.0)"
  }

  measure: catalogue_price_max {
    type: max
    sql: (${catalogue_price});;
    value_format: "#.0€;(e#.0)"
  }

  measure: catalogue_price_avg {
    type: average
    sql: (${catalogue_price});;
    value_format: "#.0€;(e#.0)"
  }

  #Exercice9
  dimension: days_difference {
    type: number
    sql: date_diff(${invoice_date}, ${order_date},DAY);;
    label: "diffDATE_EMA_exo9"
  }

  measure: days_diff_min{
    type: min
    sql: ${days_difference};;
  }

  measure: days_diff_max{
    type: max
    sql: ${days_difference};;
  }
  measure: days_diff_avg{
    type: average
    sql: ${days_difference};;
  }

  #Exercice10
  dimension: brand_logo_1 {
    type: string
    sql: CASE
      WHEN ${brand} = 'RENAULT' THEN 'https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg'
      WHEN ${brand} = 'DACIA' THEN 'https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg%22'
      WHEN ${brand} = 'ALPINE' THEN 'https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg%22'
      END;;
      html:<img src='{{value}}'  height="170" width="255"/> ;;
  }
  dimension: brand_logo_2 {
    type: string
    sql: ${brand} ;;
    html:
    {% if brand._value == "RENAULT" %}
    <img src="https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg" height="170" width="255"/>
    {% elsif brand.value == "DACIA" %}
    <img src="https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg" height="170" width="255"/>
    {% else %}
    <img src="https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg" height="170" width="255"/>
    {% endif %}
    ;;
  }
}
