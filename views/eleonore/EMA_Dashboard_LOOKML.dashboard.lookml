---
- dashboard: ema_dashboard_de_formation_og
  title: EMA_Dashboard de formation_OG
  layout: newspaper
  preferred_viewer: dashboards-next
  description: ''
  preferred_slug: 97sTgHiqiVweVHxoygsAvU
  elements:
  - title: Top 10 des modèles avec le prix catalogue le plus élevé
    name: Top 10 des modèles avec le prix catalogue le plus élevé
    model: formation_mars_2024
    explore: vin
    type: looker_wordcloud
    fields: [vin.model, vin.catalogue_price_max]
    sorts: [vin.catalogue_price_max desc]
    limit: 10
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: diff_days(${vin.order_date}, ${vin.invoice_date})
      label: diff_date
      value_format:
      value_format_name:
      dimension: diff_date
      _kind_hint: dimension
      _type_hint: number
    - category: measure
      expression:
      label: min_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: min_diff_date
      type: min
      _type_hint: number
    - category: measure
      expression:
      label: max_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: max_diff_date
      type: max
      _type_hint: number
    - category: measure
      expression:
      label: avg_diff_date
      value_format:
      value_format_name: decimal_1
      based_on: diff_date
      _kind_hint: measure
      measure: avg_diff_date
      type: average
      _type_hint: number
    - category: measure
      expression:
      label: Nb of Vehicules per Brand according to Fuel Type
      value_format:
      value_format_name:
      based_on: vin.fuel_type_replaced_EMA
      _kind_hint: measure
      measure: nb_of_vehicules_per_brand_according_to_fuel_type
      type: count_distinct
      _type_hint: number
      filters: {}
    - category: measure
      expression:
      label: "%vehicules per brand per fuel type"
      value_format:
      value_format_name: percent_1
      based_on: vin.count
      _kind_hint: measure
      measure: vehicules_per_brand_per_fuel_type
      type: count
      _type_hint: number
    - category: table_calculation
      label: Percent of Vin Count
      value_format:
      value_format_name: percent_1
      calculation_type: percent_of_column_sum
      table_calculation: percent_of_vin_count
      args:
      - vin.count
      _kind_hint: measure
      _type_hint: number
      is_disabled: true
    color_application: undefined
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 8
    col: 16
    width: 8
    height: 5
  - title: Exo1_Explore_EMA
    name: Exo1_Explore_EMA
    model: formation_mars_2024
    explore: vin
    type: single_value
    fields: [modelstartingwc]
    sorts: [modelstartingwc desc 0]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression:
      label: ModelStartingwC
      value_format:
      value_format_name:
      based_on: vin.model
      _kind_hint: measure
      measure: modelstartingwc
      type: count_distinct
      _type_hint: number
      filters:
        vin.model: C%
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: Nombre de model commençant par la lettre 'C'
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hidden_pivots: {}
    defaults_version: 1
    hidden_fields: []
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    listen:
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 2
    col: 0
    width: 5
    height: 4
  - name: Répartition des types de carburant selon la marque
    title: Répartition des types de carburant selon la marque
    model: formation_mars_2024
    explore: vin
    type: looker_pie
    fields: [vin.fuel_type, vin.count]
    filters: {}
    sorts: [vin.count desc 0]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: diff_days(${vin.order_date}, ${vin.invoice_date})
      label: diff_date
      value_format:
      value_format_name:
      dimension: diff_date
      _kind_hint: dimension
      _type_hint: number
    - category: measure
      expression:
      label: min_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: min_diff_date
      type: min
      _type_hint: number
    - category: measure
      expression:
      label: max_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: max_diff_date
      type: max
      _type_hint: number
    - category: measure
      expression:
      label: avg_diff_date
      value_format:
      value_format_name: decimal_1
      based_on: diff_date
      _kind_hint: measure
      measure: avg_diff_date
      type: average
      _type_hint: number
    - category: measure
      expression:
      label: Nb of Vehicules per Brand according to Fuel Type
      value_format:
      value_format_name:
      based_on: vin.fuel_type_replaced_EMA
      _kind_hint: measure
      measure: nb_of_vehicules_per_brand_according_to_fuel_type
      type: count_distinct
      _type_hint: number
      filters: {}
    - category: measure
      expression:
      label: "%vehicules per brand per fuel type"
      value_format:
      value_format_name: percent_1
      based_on: vin.count
      _kind_hint: measure
      measure: vehicules_per_brand_per_fuel_type
      type: count
      _type_hint: number
    - category: table_calculation
      label: Percent of Vin Count
      value_format:
      value_format_name: percent_1
      calculation_type: percent_of_column_sum
      table_calculation: percent_of_vin_count
      args:
      - vin.count
      _kind_hint: measure
      _type_hint: number
    value_labels: legend
    label_type: labPer
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    hidden_pivots: {}
    hidden_fields: [vin.count]
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 21
    col: 0
    width: 7
    height: 8
  - name: Répartition des ventes par mois par marque selon leur types de carburant
    title: Répartition des ventes par mois par marque selon leur types de carburant
    model: formation_mars_2024
    explore: vin
    type: looker_grid
    fields: [vin.brand, vin.fuel_type, vin.order_month, vin.count]
    pivots: [vin.order_month]
    fill_fields: [vin.order_month]
    filters:
      vin.order_year: '2019'
    sorts: [vin.order_month, vin.brand]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: diff_days(${vin.order_date}, ${vin.invoice_date})
      label: diff_date
      value_format:
      value_format_name:
      dimension: diff_date
      _kind_hint: dimension
      _type_hint: number
    - category: measure
      expression:
      label: min_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: min_diff_date
      type: min
      _type_hint: number
    - category: measure
      expression:
      label: max_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: max_diff_date
      type: max
      _type_hint: number
    - category: measure
      expression:
      label: avg_diff_date
      value_format:
      value_format_name: decimal_1
      based_on: diff_date
      _kind_hint: measure
      measure: avg_diff_date
      type: average
      _type_hint: number
    - category: measure
      expression:
      label: Nb of Vehicules per Brand according to Fuel Type
      value_format:
      value_format_name:
      based_on: vin.fuel_type_replaced_EMA
      _kind_hint: measure
      measure: nb_of_vehicules_per_brand_according_to_fuel_type
      type: count_distinct
      _type_hint: number
      filters: {}
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application:
      collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd
      palette_id: b0768e0d-03b8-4c12-9e30-9ada6affc357
      options:
        steps: 5
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    x_axis_zoom: true
    y_axis_zoom: true
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    series_colors: {}
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    reference_lines: [{reference_type: range, line_value: mean, range_start: max,
        range_end: mean, margin_top: deviation, margin_value: mean, margin_bottom: deviation,
        label_position: right, color: "#7CB342", label: max}, {reference_type: line,
        line_value: mean, range_start: max, range_end: min, margin_top: deviation,
        margin_value: mean, margin_bottom: deviation, label_position: center, color: "#000000",
        label: ''}, {reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#000", label: Avg}, {reference_type: line, line_value: min, range_start: max,
        range_end: min, margin_top: deviation, margin_value: mean, margin_bottom: deviation,
        label_position: right, color: "#EA4335", label: min}]
    trend_lines: []
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    hidden_pivots: {}
    minimum_column_width: 75
    series_column_widths:
      vin.brand: 97
      vin.fuel_type: 123
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 15
    col: 0
    width: 24
    height: 6
  - name: Nombre de véhicules par marque selon leur type de carburant
    title: Nombre de véhicules par marque selon leur type de carburant
    model: formation_mars_2024
    explore: vin
    type: looker_column
    fields: [vin.brand, vin.fuel_type_replaced_EMA, vin.count]
    filters: {}
    sorts: [vin.count desc 0]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: diff_days(${vin.order_date}, ${vin.invoice_date})
      label: diff_date
      value_format:
      value_format_name:
      dimension: diff_date
      _kind_hint: dimension
      _type_hint: number
    - category: measure
      expression:
      label: min_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: min_diff_date
      type: min
      _type_hint: number
    - category: measure
      expression:
      label: max_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: max_diff_date
      type: max
      _type_hint: number
    - category: measure
      expression:
      label: avg_diff_date
      value_format:
      value_format_name: decimal_1
      based_on: diff_date
      _kind_hint: measure
      measure: avg_diff_date
      type: average
      _type_hint: number
    - category: measure
      expression:
      label: Nb of Vehicules per Brand according to Fuel Type
      value_format:
      value_format_name:
      based_on: vin.fuel_type_replaced_EMA
      _kind_hint: measure
      measure: nb_of_vehicules_per_brand_according_to_fuel_type
      type: count_distinct
      _type_hint: number
      filters: {}
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: d754397b-2c05-4470-bbbb-05eb4c2b15cd
      palette_id: b0768e0d-03b8-4c12-9e30-9ada6affc357
      options:
        steps: 5
    x_axis_zoom: true
    y_axis_zoom: true
    series_colors: {}
    reference_lines: [{reference_type: range, line_value: mean, range_start: max,
        range_end: mean, margin_top: deviation, margin_value: mean, margin_bottom: deviation,
        label_position: right, color: "#7CB342", label: max}, {reference_type: line,
        line_value: mean, range_start: max, range_end: min, margin_top: deviation,
        margin_value: mean, margin_bottom: deviation, label_position: center, color: "#000000",
        label: ''}, {reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: right,
        color: "#000", label: Avg}, {reference_type: range, line_value: min, range_start: min,
        range_end: mean, margin_top: deviation, margin_value: mean, margin_bottom: deviation,
        label_position: right, color: "#EA4335", label: min}]
    trend_lines: []
    defaults_version: 1
    hidden_pivots: {}
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 21
    col: 7
    width: 17
    height: 8
  - name: Model-Version
    title: Model-Version
    model: formation_mars_2024
    explore: vin
    type: looker_grid
    fields: [vin.model, vin.version, concat_model_version]
    sorts: [vin.model]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: concat(${vin.model}, " " , ${vin.version})
      label: Concat_Model_Version
      value_format:
      value_format_name:
      dimension: concat_model_version
      _kind_hint: dimension
      _type_hint: string
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 2
    col: 16
    width: 8
    height: 4
  - title: Changement des noms de carburant
    name: Changement des noms de carburant
    model: formation_mars_2024
    explore: vin
    type: looker_grid
    fields: [vin.fuel_type, fueltypereplaced]
    sorts: [vin.fuel_type]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      description: ''
      label: FuelTypeReplaced
      value_format:
      value_format_name:
      calculation_type: group_by
      dimension: fueltypereplaced
      args:
      - vin.fuel_type
      - - label: Gasoil
          filter: DIESEL
        - label: Electrique
          filter: ELECTRIC
        - label: Essence
          filter: PETROL
        - label: GAZ
          filter: PETROL CNGGAZ
        - label: GAZ
          filter: PETROL LPG
      -
      _kind_hint: dimension
      _type_hint: string
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    truncate_column_names: false
    defaults_version: 1
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 2
    col: 11
    width: 5
    height: 4
  - name: Nomenclature des Concessions
    title: Nomenclature des Concessions
    model: formation_mars_2024
    explore: vin
    type: looker_grid
    fields: [vin.dealer_name, dealernamemodifie]
    sorts: [vin.dealer_name]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: replace(${vin.dealer_name}," ", "-")
      label: DealerNameModifie
      value_format:
      value_format_name:
      dimension: dealernamemodifie
      _kind_hint: dimension
      _type_hint: string
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 2
    col: 5
    width: 6
    height: 4
  - name: Exo6_avg_Explore_EMA
    title: Exo6_avg_Explore_EMA
    model: formation_mars_2024
    explore: vin
    type: single_value
    fields: [min_diff_date, max_diff_date, avg_diff_date]
    sorts: [avg_diff_date]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: diff_days(${vin.order_date}, ${vin.invoice_date})
      label: diff_date
      value_format:
      value_format_name:
      dimension: diff_date
      _kind_hint: dimension
      _type_hint: number
    - category: measure
      expression:
      label: min_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: min_diff_date
      type: min
      _type_hint: number
    - category: measure
      expression:
      label: max_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: max_diff_date
      type: max
      _type_hint: number
    - category: measure
      expression:
      label: avg_diff_date
      value_format:
      value_format_name: decimal_1
      based_on: diff_date
      _kind_hint: measure
      measure: avg_diff_date
      type: average
      _type_hint: number
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: true
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application:
      collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7
      palette_id: 18d0c733-1d87-42a9-934f-4ba8ef81d736
    conditional_formatting: [{type: less than, value: 100, background_color: '', font_color: "#149888",
        color_application: {collection_id: 5591d8d1-6b49-4f8e-bafa-b874d82f8eb7, palette_id: 97ce1e3f-9504-4d5c-835b-3fbaf78c084a},
        bold: false, italic: false, strikethrough: false, fields: !!null ''}]
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    truncate_header: false
    size_to_fit: true
    minimum_column_width: 75
    series_cell_visualizations:
      min_diff_date:
        is_active: false
      max_diff_date:
        is_active: false
        value_display: true
    table_theme: white
    limit_displayed_rows: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    hide_totals: false
    hide_row_totals: false
    hidden_fields: [min_diff_date, max_diff_date]
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 31
    col: 0
    width: 8
    height: 6
  - title: Exo6_max_Explore_EMA
    name: Exo6_max_Explore_EMA
    model: formation_mars_2024
    explore: vin
    type: single_value
    fields: [min_diff_date, max_diff_date, avg_diff_date]
    sorts: [avg_diff_date]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: diff_days(${vin.order_date}, ${vin.invoice_date})
      label: diff_date
      value_format:
      value_format_name:
      dimension: diff_date
      _kind_hint: dimension
      _type_hint: number
    - category: measure
      expression:
      label: min_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: min_diff_date
      type: min
      _type_hint: number
    - category: measure
      expression:
      label: max_diff_date
      value_format:
      value_format_name:
      based_on: diff_date
      _kind_hint: measure
      measure: max_diff_date
      type: max
      _type_hint: number
    - category: measure
      expression:
      label: avg_diff_date
      value_format:
      value_format_name: decimal_1
      based_on: diff_date
      _kind_hint: measure
      measure: avg_diff_date
      type: average
      _type_hint: number
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: true
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application:
      collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
      palette_id: fb7bb53e-b77b-4ab6-8274-9d420d3d73f3
    single_value_title: Maximum de jours écoulés avant l'envoi
    conditional_formatting: [{type: greater than, value: 300, background_color: "#B32F37",
        font_color: !!null '', color_application: {collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7,
          palette_id: 85de97da-2ded-4dec-9dbd-e6a7d36d5825}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    truncate_header: false
    size_to_fit: true
    minimum_column_width: 75
    series_cell_visualizations:
      min_diff_date:
        is_active: false
      max_diff_date:
        is_active: false
        value_display: true
    table_theme: white
    limit_displayed_rows: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    hide_totals: false
    hide_row_totals: false
    hidden_fields: [min_diff_date]
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 31
    col: 8
    width: 8
    height: 6
  - title: Nombre de jours entre la date de commende et la date d'envoi
    name: Nombre de jours entre la date de commende et la date d'envoi
    model: formation_mars_2024
    explore: vin
    type: looker_grid
    fields: [vin.invoice_date, vin.order_date]
    sorts: [vin.order_date desc]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: table_calculation
      expression: diff_days(${vin.order_date},${vin.invoice_date})
      label: Diff
      value_format:
      value_format_name:
      _kind_hint: dimension
      table_calculation: diff
      _type_hint: number
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 31
    col: 16
    width: 8
    height: 6
  - name: ''
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"h1","children":[{"text":"Aperçu du réseau"}],"align":"center"},{"type":"p","children":[{"text":""}],"id":"w6fh6"}]'
    rich_content_json: '{"format":"slate"}'
    row: 0
    col: 0
    width: 24
    height: 2
  - name: " (2)"
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"h1","children":[{"text":"Focus sur le temps écoulé entre
      la commande d''un véhicule et son expédition","color":"hsl(191, 65%, 15%)"}],"align":"center"},{"type":"p","children":[{"text":""}],"id":"3439x"}]'
    rich_content_json: '{"format":"slate"}'
    row: 29
    col: 0
    width: 24
    height: 2
  - name: " (3)"
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"h1","children":[{"text":"Répartitions des ventes de véhicules
      selon leur type de carburant","color":"hsl(99, 63%, 19%)"}],"align":"center"},{"type":"p","children":[{"text":""}],"id":"vgfem"}]'
    rich_content_json: '{"format":"slate"}'
    row: 13
    col: 0
    width: 24
    height: 2
  - name: " (4)"
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"children":[{"text":"Les ventes"}],"type":"h1","align":"center"},{"type":"h1","children":[{"text":""}],"id":"mxbyh"}]'
    rich_content_json: '{"format":"slate"}'
    row: 6
    col: 0
    width: 24
    height: 2
  - title: Untitled
    name: Untitled
    model: formation_mars_2024
    explore: vin
    type: single_value
    fields: [vin.catalogue_price_max]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: Max Catalogue Price
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    listen:
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
      Brand: vin.brand
    row: 8
    col: 0
    width: 8
    height: 5
  - title: Untitled
    name: Untitled (2)
    model: formation_mars_2024
    explore: vin
    type: single_value
    fields: [vin.catalogue_price_avg]
    limit: 500
    column_limit: 50
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    single_value_title: Average Catalogue Price
    hidden_pivots: {}
    defaults_version: 1
    listen:
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
      Brand: vin.brand
    row: 8
    col: 8
    width: 8
    height: 5
  filters:
  - name: Brand
    title: Brand
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: button_group
      display: inline
    model: formation_mars_2024
    explore: vin
    listens_to_filters: []
    field: vin.brand
  - name: Order Date
    title: Order Date
    type: field_filter
    default_value: 2018/01/01 to 2023/01/01
    allow_multiple_values: true
    required: false
    ui_config:
      type: day_range_picker
      display: inline
      options: []
    model: formation_mars_2024
    explore: vin
    listens_to_filters: []
    field: vin.order_date
  - name: Invoice Date
    title: Invoice Date
    type: field_filter
    default_value: 2018/01/01 to 2023/01/01
    allow_multiple_values: true
    required: false
    ui_config:
      type: day_range_picker
      display: inline
      options: []
    model: formation_mars_2024
    explore: vin
    listens_to_filters: []
    field: vin.invoice_date
