include: "/views/vin.view.lkml"
view: +vin {

#LookML 1
#Nombre unique des « model »
  measure: Count_Model_M {
    type: count_distinct
    sql: ${model} ;;
 }
#LookML 2
#Ajouter à l’exercice précédant  la fonctionnalité permettant à l’utilisateur de visualiser les modèles s’il en a besoin
#Solution 1
  set: Brand_1_M {
    fields: [model, brand]
 }
#Solution 2
  measure: Brand_2_M {
    type: count_distinct
    sql: ${brand} ;;
 }
#Solution 3
  measure: Brand_3_M {
    type: count_distinct
    drill_fields: [Brand_1_M*]
    sql: ${model}  ;;
 }
#Look ML 3
#Modifier et ajouter une colonne base sur Dealer name pour avoir « - » à la place de «   ».
  dimension: Dealder_Name_M {
    sql: REPLACE(${dealer_name}, "","-") ;;
 }
#Look ML 4
#Création d’une nouvelle colonne par Lookml base sur Fuel Type et remplacer :
  #DIESEL par Gasoil
  #ELECTRIC par Electrique
  #PETROL par Essence
  #PETROL CNGGAZ et PETROL LPG par GAZ
#Solution 1
  dimension: Fuel_Type_1_M {
    sql: REPLACE(${fuel_type},"DIESEL","Gasoil")
         REPLACE(${fuel_type},"ELECTRIC","Electrique")
         REPLACE(${fuel_type},"PETROL","Essence")
         REPLACE(${fuel_type},"PETROL CNGGAZ","Gaz")
         REPLACE(${fuel_type},"PETROL LPG","Gaz");;
 }
#Solution 2
  dimension: Fuel_Type_2_M {
    sql: CASE
          WHEN ${fuel_type} IN ("DIESEL") THEN "Gasoil"
          WHEN ${fuel_type} IN ("ELECTRIC") THEN "Electrique"
          WHEN ${fuel_type} IN ("PETROL") THEN "Essence"
          WHEN ${fuel_type} IN ("PETROL CNGGAZ", "PETROL LPG") THEN "Gaz"
          ELSE "Other"
         END;;
 }
#Look ML 5
#Concaténation de colonne model avec version par Lookml et ajouter la fonctionnalité permettant à l’utilisateur de visualiser les ‘brand, model, version, catalogue_price’ s’il en a besoin
  dimension: Model_Version_M {
    sql: CONCAT(${model}," | ",${version});;
    drill_fields: [brand, model, version, catalogue_price]
 }
#Look ML 6
#Modifier la colonne order_date type string en date pour avoir la date,  le jour de la semaine, semaine, mois et l’années.
  dimension_group: Order_Date_M {
    type: time
    timeframes: [date,week, month, year]
    convert_tz: yes
    datatype: date
    sql: ${order_date} ;;
 }
#Look ML 7
#Modifier la colonne invoice_date pour pouvoir afficher sous le forme suivante:
  dimension: Invoice_Date_M {
    sql: format_date('%A %e %b %y', ${invoice_date});;
 }
#Look ML 8
#Calculer Min, Max, average de la colonne catalogue_price par Lookml. Format en euros avec 1 décimal
  measure: Min_M {
    type: min
    sql: ${catalogue_price} ;;
    value_format: "0.0 €"
 }
  measure: Max_M {
    type: max
    sql: ${catalogue_price} ;;
    value_format: "0.0 €"
 }
  measure: Average_M {
    type: average
    sql: ${catalogue_price} ;;
    value_format: "0.0 €"
 }
#Look ML 9
#Calculer différence de jour et son Min, Max, average entre les deux colonne order_date et invoice_date par  Lookml .
  measure: Date_diff_M {
    type: number
    sql: DATE_DIFF(${invoice_date},${order_date}, DAY) ;;
 }
  measure: Min_diff_M {
    type: min
    sql: DATE_DIFF(${invoice_date},${order_date}, DAY) ;;
 }
  measure: Max_diff_M {
    type: max
    sql: DATE_DIFF(${invoice_date},${order_date}, DAY) ;;
  }
  measure: Average_diff_M {
    type: average
    sql: DATE_DIFF(${invoice_date},${order_date}, DAY) ;;
  }
#Look ML 10
#Créer une dimension dans lookml qui affiche logo de brand par rapport le choix d’utilisateur.
#Solution 1
  dimension: Logo_1_M {
    type: string
    sql: CASE
          WHEN ${brand} = 'ALPINE' THEN 'https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg'
          WHEN ${brand} = 'DACIA' THEN 'https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg'
          WHEN ${brand} = 'RENAULT' THEN 'https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg'
        END ;;
    html: <img src="{{value}}" height="170" width="255"/> ;;
  }
#Solution 2
  dimension: Logo_2_M {
    type: string
    sql: ${brand} ;;
    html: {% if brand._value == "ALPINE" %} <img src="https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg" height="170" width="255">
          {% elsif brand._value == "DACIA" %} <img src="https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg" height="170" width="255">
          {% else %} <img src="https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg" height="170" width="255">
          {% endif %} ;;
  }
}
