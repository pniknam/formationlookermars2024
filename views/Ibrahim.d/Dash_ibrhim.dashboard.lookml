---
- dashboard: dash_ibrhim
  title: dash_ibrahim
  layout: newspaper
  preferred_viewer: dashboards-next
  description: ''
  preferred_slug: lX3HPZgWjNUmssLKd03Ikr
  elements:
  - title: mars2024-2
    name: mars2024-2
    model: formation_mars_2024
    explore: vin
    type: looker_grid
    fields: [vin.model, model_start_with_c]
    filters:
      model_start_with_c: '1'
    sorts: [model_start_with_c desc]
    limit: 10
    column_limit: 50
    dynamic_fields:
    - category: measure
      expression:
      label: Model Start With C
      value_format:
      value_format_name:
      based_on: vin.model
      _kind_hint: measure
      measure: model_start_with_c
      type: count_distinct
      _type_hint: number
      filters:
        vin.model: C%
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    bar_arm_length: 25
    bar_arm_weight: 50
    bar_spinner_length: 100
    bar_spinner_weight: 50
    hidden_pivots: {}
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    show_null_points: true
    interpolation: linear
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    hidden_fields: []
    hidden_points_if_no: []
    series_labels: {}
    value_labels: legend
    label_type: labPer
    font_size: 12
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: light
    map_position: fit_data
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    groupBars: true
    labelSize: 10pt
    showLegend: true
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 12
    col: 0
    width: 13
    height: 4
  - name: Explore-Exercice7-Count-Vin-Fuel-Type
    title: Explore-Exercice7-Count-Vin-Fuel-Type
    model: formation_mars_2024
    explore: vin
    type: looker_column
    fields: [vin.brand, vin.fuel_type_replaced, vin.count]
    filters: {}
    sorts: [vin.count desc 0]
    limit: 500
    column_limit: 50
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      palette_id: 5d189dfc-4f46-46f3-822b-bfb0b61777b1
      options:
        steps: 5
    x_axis_zoom: true
    y_axis_zoom: true
    limit_displayed_rows_values:
      show_hide: hide
      first_last: first
      num_rows: 0
    hide_legend: false
    series_colors: {}
    reference_lines: [{reference_type: line, line_value: mean, range_start: max, range_end: min,
        margin_top: deviation, margin_value: mean, margin_bottom: deviation, label_position: center,
        color: "#000000"}, {reference_type: range, line_value: mean, range_start: mean,
        range_end: max, margin_top: deviation, margin_value: max, margin_bottom: deviation,
        label_position: right, color: "#7CB342", label: Average}, {reference_type: range,
        line_value: mean, range_start: min, range_end: mean, margin_top: deviation,
        margin_value: mean, margin_bottom: deviation, label_position: left, color: "#EA4335",
        label: Min}]
    trend_lines: []
    show_dropoff: false
    hidden_pivots: {}
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 6
    col: 3
    width: 8
    height: 6
  - name: Explore-Exercice6-Red-Color-Max-Date-Diff
    title: Explore-Exercice6-Red-Color-Max-Date-Diff
    model: formation_mars_2024
    explore: vin
    type: single_value
    fields: [vin.max_date_diff]
    limit: 500
    column_limit: 50
    dynamic_fields:
    - category: dimension
      expression: replace(${vin.dealer_name}," ","-")
      label: Dealer Name Modif2
      value_format:
      value_format_name:
      dimension: dealer_name_modif2
      _kind_hint: dimension
      _type_hint: string
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: true
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: ''
    conditional_formatting: [{type: greater than, value: 300, background_color: "#ff060f",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    hidden_pivots: {}
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    show_null_points: true
    interpolation: linear
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 0
    col: 16
    width: 8
    height: 6
  - name: Explore-Exercice6-Green-Color-Average-Date-Diff
    title: Explore-Exercice6-Green-Color-Average-Date-Diff
    model: formation_mars_2024
    explore: vin
    type: single_value
    fields: [vin.average_date_diff]
    limit: 500
    column_limit: 50
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: true
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    conditional_formatting: [{type: less than, value: 100, background_color: "#7CB342",
        font_color: '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    hidden_pivots: {}
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 0
    col: 8
    width: 8
    height: 6
  - name: Exercice8
    title: Exercice8
    model: formation_mars_2024
    explore: vin
    type: looker_grid
    fields: [vin.count, vin.order_month, vin.brand, vin.fuel_type_replaced]
    pivots: [vin.order_month]
    fill_fields: [vin.order_month]
    filters:
      vin.order_year: '2019'
    sorts: [vin.order_month desc, vin.brand 0]
    limit: 500
    column_limit: 50
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    hidden_pivots: {}
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    minimum_column_width: 75
    series_column_widths:
      vin.brand: 75
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 16
    col: 0
    width: 24
    height: 5
  - name: Exercice9
    title: Exercice9
    model: formation_mars_2024
    explore: vin
    type: looker_pie
    fields: [vin.fuel_type_replaced, vin.count]
    filters: {}
    sorts: [vin.count desc 0]
    limit: 500
    column_limit: 50
    value_labels: labels
    label_type: labPer
    hidden_pivots: {}
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 6
    col: 15
    width: 8
    height: 6
  - name: Exercice10
    title: Exercice10
    model: formation_mars_2024
    explore: vin
    type: looker_wordcloud
    fields: [vin.max_catalogue_price, vin.model]
    sorts: [vin.max_catalogue_price desc 0]
    limit: 10
    column_limit: 50
    color_application: undefined
    hidden_pivots: {}
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 0
    col: 0
    width: 8
    height: 6
  - name: Exercice11
    title: Exercice11
    model: formation_mars_2024
    explore: vin
    type: looker_grid
    fields: [vin.invoice_date, vin.order_date, vin.date_diff]
    sorts: [vin.date_diff desc]
    limit: 500
    column_limit: 50
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    hidden_pivots: {}
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    listen:
      Brand: vin.brand
      Order Date: vin.order_date
      Invoice Date: vin.invoice_date
    row: 12
    col: 13
    width: 10
    height: 4
  filters:
  - name: Brand
    title: Brand
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: button_group
      display: inline
    model: formation_mars_2024
    explore: vin
    listens_to_filters: []
    field: vin.brand
  - name: Order Date
    title: Order Date
    type: field_filter
    default_value: 2024/03/07 to 2024/03/08
    allow_multiple_values: true
    required: false
    ui_config:
      type: relative_timeframes
      display: inline
      options: []
    model: formation_mars_2024
    explore: vin
    listens_to_filters: []
    field: vin.order_date
  - name: Invoice Date
    title: Invoice Date
    type: field_filter
    default_value: 7 day
    allow_multiple_values: true
    required: false
    ui_config:
      type: relative_timeframes
      display: inline
      options: []
    model: formation_mars_2024
    explore: vin
    listens_to_filters: []
    field: vin.invoice_date
