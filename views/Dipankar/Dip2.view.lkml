include: "/views/vehicules.view.lkml"
include: "/views/caracteristiques.view.lkml"
include: "/views/usagers.view.lkml"

view: +usagers {
  dimension: age_group_usagers {
    type: string
    sql: CASE
      WHEN ${caracteristiques.an}-${an_nais} <= 15 THEN 'Enfant'
      WHEN ${caracteristiques.an}-${an_nais} > 15 AND ${caracteristiques.an}-${an_nais} <= 24 THEN 'Adolescent'
      WHEN ${caracteristiques.an}-${an_nais} > 24 AND ${caracteristiques.an}-${an_nais} <= 64 THEN 'Adulte'
      ELSE 'Aîné'
    END ;;
  }

  dimension: regroup_user_age_accident_2 {
    case: {
      when: {
        sql: ${caracteristiques.an}-${an_nais} <= 14 ;;
        label: "Enfants"
      }
    }
  }
}
