include: "/views/vin.view.lkml"
view: +vin{
measure: countbrand {
type: count_distinct
sql: ${brand};;
}
measure: countmodel {
  type: count_distinct
  sql: ${model};;
  drill_fields: [model]



}
dimension: dealer_name1{
type: string
sql: replace(${dealer_name}, " ","_");;
}


dimension: FuelType {
  type: string
  sql:

case
    when ${fuel_type} IN ('Diesel') then label: 'Gasoil'
    when ${fuel_type} IN ('ELECTRIC') then label: 'Electrique'
    when ${fuel_type} IN ('PETROL') then label: 'Essence' end ;;}



  dimension: combined_info {
    type: string
    sql: CONCAT(${brand}, ' ', ${model}, ' ', ${version}, ' ', ${catalogue_price}) ;;
    drill_fields: [brand,version,model,catalogue_price]


  }

  dimension: invoiceDATE {
    sql: FORMAT_DATETIME('%A %d %B %Y', ${invoice_date});;
    label: "invoiceDATE_EMA_exo7"
  }




    measure: min_catalogue_price {
      type: min
      sql: ${catalogue_price} ;;
      value_format: "$#.00;($#.00)"
    }

    measure: max_catalogue_price {
      type: max
      sql: ${catalogue_price} ;;
      value_format: "$#.00;($#.00)"}


    measure: average_catalogue_price {
      type: average
      sql: ${catalogue_price} ;;
      value_format: "$#.00;($#.00)"}

  dimension: days_difference {
    type: number
    sql: date_diff(${invoice_date}, ${order_date}) ;;
  }

  measure: min_days_difference {
    type: min
    sql: ${days_difference} ;;
  }



  measure: max_days_difference {
    type: max
    sql: ${days_difference} ;;
  }


  measure: average_days_difference {
    type: average
    sql: ${days_difference} ;;
    value_format: "0.0" # This formats the number to have one decimal place
  }



  #Exercice10
  dimension: logo1 {
    type: string
    sql:  CASE
          WHEN ${brand} = 'ALPINE'  then 'https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg'
          WHEN ${brand} = 'DACIA'   then 'https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg'
          WHEN ${brand} = 'RENAULT' then 'https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg'
          END;;
    html: <img src="{{value}}" height="170" width="255"/> ;;
  }

  dimension: logo2 {
    type: string
    sql: ${brand};;
    html:
    {% if brand._value == "ALPINE" %}
    <img src="https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg" height="170" width="255">
    {% elsif brand._value == "DACIA" %}
    <img src="https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg" height="170" width="255">
    {% else  %}
    <img src="https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg" height="170" width="255">
    {% endif %} ;;
  }

  dimension: dealer_name_modified {
    type: string
    sql: CASE WHEN ${TABLE}.dealer_name = '' THEN '-' ELSE ${TABLE}.dealer_name END ;;
  }








}
