include: "/views/usagers.view.lkml"
include: "/views/caracteristiques.view.lkml"


view: +usagers {

#Exercice3
dimension: age_usager {
  sql: ${caracteristiques.an} - ${an_nais} ;;
}

  dimension: tranche_age_usager {
    sql: CASE
          WHEN ${age_usager} <= 15 THEN 'Enfants'
          WHEN ${age_usager} >15 and ${age_usager} <= 24 THEN 'Adolescents'
          WHEN ${age_usager} >24 and ${age_usager} <= 64 THEN 'Adultes'
          --WHEN ${age_usager} > 64 THEN 'Aînés'
          ELSE 'Aînés'
    END
    ;;

  }

}
