include: "/views/caracteristiques.view.lkml"

view: +caracteristiques {

#Exercice2 façon Pedram
  dimension_group: current {
    type:  time
    timeframes: [
      raw,
      date,
      day_of_week,
      week,
      month,
      year
    ]
    convert_tz: no
    datatype: date
    sql:  current_date() ;;
  }

  dimension: anciennete_acc {
    type: number
    sql: ${current_year}-${an} ;;
  }

#Exercice2 façon Benoit

dimension: annee_actuel {
  sql:  Extract(YEAR FROM CURRENT_DATE()) as annee_actuel ;;
}

dimension: anciennete_accident{
  sql: ${annee_actuel} - ${an}  ;;
}


}
