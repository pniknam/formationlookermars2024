include: "/views/vin.view.lkml"

view: +vin {

#ADD Fonction Set // # #Exercice2
  set: brand_fonction {
    fields: [model, brand]
  }

  measure: brand_unique {
    type: count_distinct
    sql: ${brand}  ;;
  }

#Exercice1
  measure: model_unique {
    type: count_distinct
    drill_fields: [brand_fonction*]  # #Exercice2
    sql: ${model}  ;;
  }

  #Exercice3
  dimension: Dealer_Name_Modif {
    sql:  REPLACE(${dealer_name}, " ", "-") ;;
  }

  #Exercice4
  dimension: fuel_type_replaced {
    sql: CASE
         WHEN fuel_type IN ("DIESEL") THEN "Gasoil"
         WHEN fuel_type IN ("ELECTRIC") THEN "Electrique"
         WHEN fuel_type IN ("PETROL") THEN "Essence"
         WHEN fuel_type IN ("PETROL CNGGAZ", "PETROL LPG") THEN "Gaz"
         ELSE "Other"
END ;;
  }

  #Exercice5
  dimension: model_version {
    sql:  CONCAT(${model}, " ", ${version}) ;;
    drill_fields: [brand, model, version, catalogue_price]
  }

  #Exercice7
  dimension: invoice_date_format {
    sql: format_date('%A %e %b %y',${invoice_date}) ;;
  }

  #Exercice8
  measure: min_catalogue_price {
    type: min
    sql: ${catalogue_price} ;;
    value_format: "0.0€"
  }

  measure: max_catalogue_price {
    type: max
    sql: ${catalogue_price} ;;
    value_format: "0.0€"
  }

  measure: average_catalogue_price {
    type: average
    sql: ${catalogue_price} ;;
    value_format: "0.0€"
  }

  #Exercice9
  measure: date_diff {
    type: number
    sql: DATE_DIFF(${invoice_date},${order_date}, DAY) ;;
  }

  measure: min_date_diff {
    type: min
    sql: DATE_DIFF(${invoice_date},${order_date}, DAY) ;;
  }

  measure: max_date_diff {
    type: max
    sql: DATE_DIFF(${invoice_date},${order_date}, DAY) ;;
  }

  measure: average_date_diff {
    type: average
    sql: DATE_DIFF(${invoice_date},${order_date}, DAY) ;;
    value_format: "0.0"
  }

  #Exercice10
  dimension: logo1 {
    type: string
    sql:  CASE
          WHEN ${brand} = 'ALPINE'  then 'https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg'
          WHEN ${brand} = 'DACIA'   then 'https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg'
          WHEN ${brand} = 'RENAULT' then 'https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg'
          END;;
          html: <img src="{{value}}" height="170" width="255"/> ;;
  }

  dimension: logo2 {
    type: string
    sql: ${brand};;
    html:
    {% if brand._value == "ALPINE" %}
    <img src="https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg" height="170" width="255">
    {% elsif brand._value == "DACIA" %}
    <img src="https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg" height="170" width="255">
    {% else  %}
    <img src="https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg" height="170" width="255">
    {% endif %} ;;
  }


}
