include: "/views/vin.view.lkml"
view: +vin {
  measure: countBrand {
    type: count_distinct
    # sql: ${TABLE}.brand ;;
    sql: ${brand} ;;
    label: "Unique Count Brand "
  }
}
