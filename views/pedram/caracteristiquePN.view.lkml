include: "/views/caracteristiques.view.lkml"
view: +caracteristiques{

  dimension_group: current {
    type:  time
    timeframes: [
      raw,
      date,
      day_of_week,
      week,
      month,
      year
    ]
    convert_tz: no
    datatype: date
    sql:  current_date() ;;
  }

  dimension: anciennete_acc {
    type: number
    sql: ${current_year}-${an} ;;
  }

  dimension: age_usager {
    type: number
    sql: ${an} - ${usagers.an_nais}
      ;;
  }

  dimension: age_usager_group {
    type: string
    sql: CASE
          WHEN ${age_usager} <= 15 THEN 'Enfants'
          WHEN ${age_usager} >15 and ${age_usager} <= 24 THEN 'Adolescents'
          WHEN ${age_usager} >24 and ${age_usager} <= 64 THEN 'Adultes'
          --WHEN ${age_usager} > 64 THEN 'Aînés'
          ELSE 'Aînés'
        END
          ;;
  }
}
