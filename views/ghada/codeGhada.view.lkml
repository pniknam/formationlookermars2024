include: "/views/vin.view.lkml"
view: +vin {
  measure : countBrand {
    type:  count_distinct
    sql:  ${brand} ;;
  }

  measure : countModel{
    type:  count_distinct
    sql:  ${model};;
    drill_fields: [drill*]}
  measure: countUnique {
    type: count_distinct
    sql:  ${dealer_name} ;;
  }
  set: drill{
    fields: [ model, brand]
  }
  dimension: dealer_name3 {
    type:  string
    sql: REPLACE(${dealer_name}, " ", "-") ;;
  }
  dimension: fuel_typeEX5 {
    case: {
      when: {
        sql:  ${fuel_type} = "DIESEL" ;;
        label: "GASOIL"
      }
      when: {
        sql:  ${TABLE}.status = "ELECTRIC" ;;
        label: "Electrique"
      }
      when: {
        sql:  ${fuel_type} = "PETROL_CNGGAZ" ;;
        label: "GAZ"
      }
      when: {
        sql:  ${fuel_type} = "PETROL_LPG" ;;
        label: "GAZ"
      }
  }
  }
  dimension: ConcatModel {
    type: string
    sql:  concat(${brand},"-",${model},"-",${version},"-", ${catalogue_price}) ;;
  }
  dimension: ConcatModelOptimizedPerFilter {
    type: string
    sql:  concat(${model},"-",${version}) ;;
    drill_fields: [brand, model, version,catalogue_price]
  }
 dimension: invoiceDATE {

  sql: FORMAT_DATETIME('%A %d %B %Y', ${invoice_date});;
  label: "invoiceDATE_EMA_exo7"
}
measure:  minCataloguePrice {
  type:  number
  sql: MIN(${catalogue_price}) ;;
  label: "Min Catalogue price"
  value_format: "€#, ##0.0"
}
  measure:  maxCataloguePrice {
    type:  number
    sql: MAX(${catalogue_price}) ;;
    label: "Max Catalogue price"
    value_format: "€#, ##0.0"
  }
  measure:  avgCataloguePrice {
    type:  number
    sql: AVG(${catalogue_price}) ;;
    label: "Avg Catalogue price"
    value_format: "€#, ##0.0"
  }
 dimension: diff_days {
  type: number
  sql: date_diff(${invoice_date},${order_date}, DAY) ;;
  label: "Difference de jour"
}
  measure: min_diff_days {
    type: min
    sql: ${diff_days} ;;
    label: "Min Difference de jour"
  }
measure: max_diff_days {
  type: max
  sql: ${diff_days} ;;
  label: "Max Difference de jour"
}
measure: avg_diff_days {
  type: average
  sql: ${diff_days} ;;
  label: "Average Difference de jour"
}
dimension: logo {
  type:  string
  sql:  ${brand} ;;
  html:
  {% if brand._value == "ALPINE" %}
  <img src="https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg" height="170" width="255"/>
  {% elsif brand._value == "DACIA" %}
  <img src="https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg" height="170" width="255"/>
  {% else %}
  <img src="https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg" height="170" width="255"/>
  {% endif %};;
}

dimension: logo2  {
  type:  string
  sql:  case
  WHEN ${brand} ="ALPINE" then "https://www.retro-laser.com/wp-content/uploads/2021/12/2021-12-13-at-08-17-16.jpg%22"
  WHEN ${brand} ="DACIA" then "https://upload.wikimedia.org/wikipedia/fr/4/4d/Logo_Dacia.svg%22"
  WHEN ${brand} ="RENAULT" then "https://upload.wikimedia.org/wikipedia/commons/4/49/Renault_2009_logo.svg%22"
  END;;
  html:  <img src='{(value)} ;;


}


  }
