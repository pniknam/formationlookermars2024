connection: "formation_looker"

# include all the views
include: "/views/**/*.view.lkml"
include: "/views/benoit/Mars_formation.dashboard.lookml"

datagroup: formation_mars_2024_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: formation_mars_2024_default_datagroup

explore: vin {}

explore: vehicules {
  join: caracteristiques {
    sql_on: ${vehicules.num_acc} = ${caracteristiques.num_acc}   ;;
    type: left_outer
    relationship: many_to_one
  }
  join: lieux {
    sql_on: ${vehicules.num_acc} = ${lieux.num_acc} ;;
    type: left_outer
    relationship: many_to_one
  }
  join: usagers {
    sql_on: ${vehicules.num_acc} = ${usagers.num_acc} ;;
    type: left_outer
    relationship: many_to_many
  }
}
